package main

import (
	"github.com/golang/glog"
	"os"
	"path/filepath"

	"gitlab.com/sthussey/async-dashboard/pkg/service"
)

func main() {
	defer glog.Flush()

	cmdName := filepath.Base(os.Args[0])

	app := service.Create(cmdName)
	if app.Configure() {
		app.Start()
	}
}
