package service

import (
	"flag"
	"fmt"
)

type DashboardApp struct {
	Conf *DashboardConfig
}

type DashboardConfig struct {
	NoAuth bool
}

func createDashboardApp() *DashboardApp {
	app := DashboardApp{}
	return &app
}

func (a *DashboardApp) Configure() bool {
	conf := DashboardConfig{}

	help := flag.Bool("h", false, "Show help and exit.")
	flag.BoolVar(&conf.NoAuth, "noauth", false, "Disable authentication.")

	flag.Parse()

	if *help {
		flag.PrintDefaults()
		return false
	} else {
		a.Conf = &conf
		return true
	}
}

func (a *DashboardApp) Start() {
	conf := a.Conf

	if conf.NoAuth {
		fmt.Printf("You fool, never run NoAuth!\n")
	} else {
		fmt.Printf("Have no support for authentication.\n")
	}

	return
}
