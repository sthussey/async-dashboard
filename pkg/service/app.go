package service

type App interface {
	Configure() bool
	Start()
}

func Create(basecmd string) App {
	switch basecmd {
	case "async-dashboard":
		app := createDashboardApp()
		return app
	}
	return nil
}
